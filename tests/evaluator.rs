use std::collections::HashMap;

extern crate regex;
use regex::Regex;

extern crate awk;
use awk::evaluator::*;
use awk::ast::*;
use awk::values::Value::{self, *};

#[test]
fn it_assigns_variables_when_a_pattern_matches() {
    use awk::ast::Statement::*;
    use awk::ast::Condition::*;
    use awk::ast::Expression::*;

    let mut e = Evaluator {
        actions: vec![
            Action {
                condition: Match(Regex::new("r").unwrap()),
                block: vec![
                    Assignment("hest".to_string(),Box::new(Constant(Numeric(23))))
                ]
            }
        ],
        state: EvaluatorState::new()
    };

    one_line(&mut e, "erk".to_string());

    assert_eq!(*e.state.variables.get("hest").unwrap(), 23)
}

#[test]
fn it_assigns_no_variables_when_a_pattern_does_not_match() {
    use awk::ast::Statement::*;
    use awk::ast::Condition::*;
    use awk::ast::Expression::*;

    let mut e = Evaluator {
        actions: vec![
            Action {
                condition: Match(Regex::new("bork").unwrap()),
                block: vec![
                    Assignment("hest".to_string(),Box::new(Constant(Numeric(23))))
                ]
            }
        ],
        state: EvaluatorState::new()
    };

    one_line(&mut e, "erk".to_string());

    assert_eq!(e.state.variables.get("hest"), None)
}

#[test]
fn it_assigns_variables_from_fieldrefs() {
    use awk::ast::Statement::*;
    use awk::ast::Condition::*;
    use awk::ast::Expression::*;

    let mut e = Evaluator {
        actions: vec![
            Action {
                condition: All,
                block: vec![
                    Assignment("hest".to_string(),Box::new(FieldRef(2)))
                ]
            }
        ],
        state: EvaluatorState::new()
    };

    one_line(&mut e, "23 24".to_string());

    assert_eq!(*e.state.variables.get("hest").unwrap(), 24)
}

#[test]
fn it_assigns_variables_from_complex_exprs() {
    use awk::ast::Statement::*;
    use awk::ast::Condition::*;
    use awk::ast::Expression::*;

    let mut e = Evaluator {
        actions: vec![
            Action {
                condition: All,
                block: vec![
                    Assignment("hest".to_string(),Box::new(Plus(Box::new(Constant(Numeric(2))),Box::new(FieldRef(2)))))
                ]
            }
        ],
        state: EvaluatorState::new()
    };

    one_line(&mut e, "23 22".to_string());

    assert_eq!(*e.state.variables.get("hest").unwrap(), 24)
}

#[test]
fn it_assigns_a_variable_when_an_expr_condition_matches() {
    use awk::ast::Statement::*;
    use awk::ast::Condition::*;
    use awk::ast::Expression::*;

    let mut e = Evaluator {
        actions: vec![
            Action {
                condition: Expr(Box::new(LessThan(Box::new(Constant(Numeric(2))), Box::new(Constant(Numeric(3)))))),
                block: vec![
                    Assignment("hest".to_string(),Box::new(Constant(Numeric(23))))
                ]
            }
        ],
        state: EvaluatorState::new()
    };

    one_line(&mut e, "erk".to_string());

    assert_eq!(*e.state.variables.get("hest").unwrap(), 23)
}

#[test]
fn it_assigns_no_variable_when_an_expr_condition_does_not_match() {
    use awk::ast::Statement::*;
    use awk::ast::Condition::*;
    use awk::ast::Expression::*;

    let mut e = Evaluator {
        actions: vec![
            Action {
                condition: Expr(Box::new(GreaterThan(Box::new(Constant(Numeric(2))), Box::new(Constant(Numeric(3)))))),
                block: vec![
                    Assignment("hest".to_string(),Box::new(Constant(Numeric(23))))
                ]
            }
        ],
        state: EvaluatorState::new()
    };

    one_line(&mut e, "erk".to_string());

    assert_eq!(e.state.variables.get("hest"), None)
}
