extern crate awk;

extern crate nom;

extern crate regex;

use nom::types::CompleteStr;

use awk::ast::Condition::*;
use awk::ast::Expression::*;
use awk::ast::Statement::*;
use awk::ast::Action;

use awk::parser::*;
use awk::values::Value::*;

use regex::Regex;

#[test]
fn it_parses_simple_conditions() {
    assert_eq!(parse_condition(CompleteStr("BEGIN")), Ok((CompleteStr(""), Begin)));
    assert_eq!(parse_condition(CompleteStr("END")), Ok((CompleteStr(""), End))); 
    assert_eq!(parse_condition(CompleteStr("/hest/")), Ok((CompleteStr(""), Match(Regex::new("hest").unwrap())))); 
    assert_eq!(parse_condition(CompleteStr("  /hest/")), Ok((CompleteStr(""), Match(Regex::new("hest").unwrap())))); 
}

#[test]
fn it_parses_constants() {
    assert_eq!(parse_expression(CompleteStr("23")), Ok((CompleteStr(""), Constant(Numeric(23)))));
}

#[test]
fn it_parses_fieldrefs() {
    assert_eq!(parse_expression(CompleteStr("$23")), Ok((CompleteStr(""), FieldRef(23))));
}

#[test]
fn it_parses_varrefs() {
    assert_eq!(parse_expression(CompleteStr("a23")), Ok((CompleteStr(""), VarRef("a23".to_string()))));
}

#[test]
fn it_parses_infixes() {
    assert_eq!(parse_expression(CompleteStr("23+4")), 
               Ok((CompleteStr(""), 
                   Plus(
                       Box::new(Constant(Numeric(23))),
                       Box::new(Constant(Numeric(4)))))));
    assert_eq!(parse_expression(CompleteStr("2-$1")), 
               Ok((CompleteStr(""), 
                   Minus(
                       Box::new(Constant(Numeric(2))),
                       Box::new(FieldRef(1))))));
    assert_eq!(parse_expression(CompleteStr("2>$1")), 
               Ok((CompleteStr(""), 
                   LessThan(
                       Box::new(Constant(Numeric(2))),
                       Box::new(FieldRef(1))))));
    assert_eq!(parse_expression(CompleteStr("23<4")), 
               Ok((CompleteStr(""), 
                   GreaterThan(
                       Box::new(Constant(Numeric(23))),
                       Box::new(Constant(Numeric(4)))))));
}

#[test]
fn it_parses_ranges() {
    assert_eq!(parse_condition(CompleteStr("/abc/,/cde/")),
               Ok((CompleteStr(""),
               Range(Regex::new("abc").unwrap(), 
                     Regex::new("cde").unwrap()))));

    assert_eq!(parse_condition(CompleteStr("  /abc/, /cde/")),
               Ok((CompleteStr(""),
               Range(Regex::new("abc").unwrap(), 
                     Regex::new("cde").unwrap()))));
}

#[test]
fn it_parses_references() {
    assert_eq!(parse_expression(CompleteStr("hest123")),
               Ok((CompleteStr(""),
               VarRef("hest123".to_string()))));
    /* TODO correct whitespace sensitivity?
     * assert_eq!(parse_expression(CompleteStr("1hest")),
               Error((CompleteStr("1hest"),
               VarRef("hest".to_string())))); */
    assert_eq!(parse_expression(CompleteStr("hest")),
               Ok((CompleteStr(""),
               VarRef("hest".to_string()))));
}

#[test]
fn it_parses_assignments() {
    assert_eq!(parse_statement(CompleteStr("hest123 = 4;")),
               Ok((CompleteStr(""),
               Assignment("hest123".to_string(),
                                     Box::new(Constant(Numeric(4)))))));
}

#[test]
fn it_parses_prints() {
    assert_eq!(parse_statement(CompleteStr("print hest;")),
               Ok((CompleteStr(""),
                   Print(Box::new(VarRef("hest".to_string()))))));
}

#[test]
fn it_parses_blocks() {
    assert_eq!(parse_statement_block(CompleteStr("{ print hest; print ko; }")),
               Ok((CompleteStr(""),
                   vec![Print(Box::new(VarRef("hest".to_string()))),
                        Print(Box::new(VarRef("ko".to_string())))])))
}

#[test]
fn it_parses_begin_actions() {
    assert_eq!(parse_action(CompleteStr("BEGIN { print hest; print ko; }")),
               Ok((CompleteStr(""),
                   Action { 
                       condition: Begin,
                       block: vec![Print(Box::new(VarRef("hest".to_string()))),
                                  Print(Box::new(VarRef("ko".to_string())))]
                   })))
}

#[test]
fn it_parses_catch_all_actions() {
    assert_eq!(parse_action(CompleteStr("{ print hest; print ko; }")),
               Ok((CompleteStr(""),
                   Action { 
                       condition: All,
                       block: vec![Print(Box::new(VarRef("hest".to_string()))),
                                  Print(Box::new(VarRef("ko".to_string())))]
                   })))
}

#[test]
fn it_parses_range_actions() {
    assert_eq!(parse_action(CompleteStr("/foo/, /bar/ { print hest; print ko; }")),
               Ok((CompleteStr(""),
                   Action { 
                       condition: Range(Regex::new("foo").unwrap(), 
                                        Regex::new("bar").unwrap()),
                       block: vec![Print(Box::new(VarRef("hest".to_string()))),
                                  Print(Box::new(VarRef("ko".to_string())))]
                   })))
}

#[test]
fn it_parses_multiple_actions() {
    assert_eq!(parse_actions(CompleteStr("BEGIN {print hest;}\n{ print ko; }\n")),
               Ok((CompleteStr(""),
                   vec! [Action {
                       condition: Begin,
                       block: vec![Print(Box::new(VarRef("hest".to_string())))]
                   }, Action {
                       condition: All,
                       block: vec![Print(Box::new(VarRef("ko".to_string())))]
                   }])));
    assert_eq!(parse_actions(CompleteStr("BEGIN { print $0; }\nEND { print ko; }\n")),
               Ok((CompleteStr(""),
                   vec! [Action {
                       condition: Begin,
                       block: vec![Print(Box::new(FieldRef(0)))]
                   }, Action {
                       condition: End,
                       block: vec![Print(Box::new(VarRef("ko".to_string())))]
                   }])));
}
