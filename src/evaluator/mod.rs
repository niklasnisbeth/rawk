use std::collections::HashMap;
use std::io::{BufRead};
use std::str::FromStr;

use ast::*;
use values::Value::{self, *};

#[derive(Debug)]
pub struct EvaluatorState {
    pub variables: HashMap<String,Value>,
    pub fields: Vec<String>, 
}

impl EvaluatorState {
    pub fn new() -> EvaluatorState { 
        EvaluatorState {
            variables: HashMap::new(),
            fields: Vec::new(),
        }
    }
}

#[derive(Debug)]
pub struct Evaluator { 
    pub actions: Vec<Action>,
    pub state: EvaluatorState
}

fn conditional<F>(l: &Expression, 
                  r: &Expression, 
                  f: F, 
                  state: &EvaluatorState) -> Value
where F: Fn(Value,Value) -> bool {
    let r1 = state.evaluate_expr(l);
    let r2 = state.evaluate_expr(r);
    if f(r1,r2) {
        Value::Numeric(1)
    } else {
        Value::Numeric(0)
    }
}

impl EvaluatorState {
    pub fn condition_matches(&self,
                             c: &Condition, 
                             line: &str) -> bool {
        use ast::Condition::*;
        match c {
            All => true,
            Begin => false,
            End => false,
            Match(r) => r.is_match(line),
            Expr(e) => {
                match self.evaluate_expr(e) {
                    Numeric(i) if i > 0 => true,
                    Numeric(_) => false,
                }
            },
            Range(_,_) => false,
        }
    }

    fn evaluate_expr(&self, e: &Expression) -> Value {
        use ast::Expression::*;
        match e {
            Constant(i) => *i,
            VarRef(v) => *self.variables.get(v).unwrap(),
            FieldRef(v) => Value::from_str(&self.fields[*v as usize - 1]).unwrap(), // TODO don't shift this here..
            Plus(l,r) => self.evaluate_expr(l) + self.evaluate_expr(r),
            Minus(l,r) => self.evaluate_expr(l) - self.evaluate_expr(r), 
            LessThan(l,r) => conditional(l, r, |x,y| x < y, &self),
            GreaterThan(l,r) => conditional(l, r, |x,y| x > y, &self)
        }
    }

    fn exec_statement(&mut self, s: &Statement) {
        use ast::Statement::*;
        match s {
            Print(e) => {
                let res = self.evaluate_expr(e); 
                println!("{}", res);
            },
            Assignment(s, e) => {
                let res = self.evaluate_expr(e); 
                self.variables.insert(s.to_string(), res);
            },
        }
    }

    pub fn do_action(&mut self, block: &Vec<Statement>) {
        for statement in block.iter() {
            self.exec_statement(&statement)
        }
    }
}

pub fn one_line(e: &mut Evaluator, line: String) {
    e.state.fields = Vec::new();
    for f in line.split(" ") {
        e.state.fields.push(f.to_owned());
    }
    for action in e.actions.iter() {
        if e.state.condition_matches(&action.condition, &line) {
            e.state.do_action(&action.block);
        }
    }
}

fn begin(e: &mut Evaluator) {
    for action in e.actions.iter() {
        match action.condition {
            Condition::Begin => { e.state.do_action(&action.block); () },
            _ => ()
        }
    }
}

fn end(e: &mut Evaluator) {
    for action in e.actions.iter() {
        match action.condition {
            Condition::Begin => { e.state.do_action(&action.block); () },
            _ => ()
        }
    }
}

pub fn process_lines(e: &mut Evaluator, buf: &mut BufRead) {
    begin(e);
    for line in buf.lines() {
        one_line(e, line.unwrap())
    }
    end(e);
}
