use std::fmt;
use std::ops::{Add, Sub};

use std::str::FromStr;
use std::num::ParseIntError;

#[derive(Debug,Copy,Clone)]
pub enum Value {
    Numeric(i64),
}

impl FromStr for Value {
    type Err = ParseIntError;
    fn from_str(s: &str) -> Result<Value,ParseIntError> {
        match i64::from_str(s) {
            Ok(i) => Ok(Value::Numeric(i)),
            Err(x) => Err(x)
        }
    }
}

impl From<String> for Value {
    fn from (s: String) -> Self {
        Value::from_str(&s).unwrap()
    }
}

impl Add for Value {
    type Output = Value; 

    fn add(self, other: Value) -> Value {
        use self::Value::*;
        match (self, other) {
            (Numeric(l), Numeric(r)) => Numeric(l+r)
        }
    }
}

impl Sub for Value {
    type Output = Value; 

    fn sub(self, other: Value) -> Value {
        use self::Value::*;
        match (self, other) {
            (Numeric(l), Numeric(r)) => Numeric(l-r)
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Value) -> bool {
        use self::Value::*;
        match (self, other) {
            (Numeric(l), Numeric(r)) => l == r
        }
    }
}

impl PartialEq<i64> for Value {
    fn eq(&self, other: &i64) -> bool {
        use self::Value::*;
        match (self, other) {
            (Numeric(l), r) => l == r
        }
    }
}
 
use std::cmp::Ordering;

impl PartialOrd for Value {
    fn partial_cmp(&self, other: &Value) -> Option<Ordering> {
        use self::Value::*;
        match (self, other) {
            (Numeric(l), Numeric(r)) => Some(l.cmp(r))
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Value::*;
        match self {
            Numeric(n) => write!(f, "{}", n)
        }
    }
}
