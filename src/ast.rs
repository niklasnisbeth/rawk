use regex::Regex;

use values::Value;

#[derive(Debug)]
pub enum Condition {
    All,
    Begin,
    End,
    Match(Regex),
    Expr(Box<Expression>),
    Range(Regex,Regex)
}

impl PartialEq for Condition {
    fn eq(&self, other: &Condition) -> bool {
        use self::Condition::*;
        match (self, other) {
            (All, All) => true,
            (Begin, Begin) => true,
            (End, End) => true,
            (Match(r1), Match(r2)) => r1.as_str() == r2.as_str(),
            (Expr(e1), Expr(e2)) => e1 == e2,
            (Range(r1a,r1b), Range(r2a, r2b)) => r1a.as_str() == r2a.as_str() && r1b.as_str() == r2b.as_str(),
            _ => false
        }
    }
}

#[derive(Debug,PartialEq)]
pub enum Expression {
    Constant(Value),
    FieldRef(i64),
    VarRef(String),
    Plus(Box<Expression>,Box<Expression>),
    Minus(Box<Expression>,Box<Expression>),
    LessThan(Box<Expression>,Box<Expression>),
    GreaterThan(Box<Expression>,Box<Expression>) 
}

#[derive(Debug,PartialEq)]
pub enum Statement {
    Print(Box<Expression>),
    Assignment(String,Box<Expression>)
}

#[derive(Debug,PartialEq)]
pub struct Action {
    pub condition: Condition, 
    pub block: Vec<Statement>
}
