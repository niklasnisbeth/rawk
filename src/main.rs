extern crate awk;

use std::io::{self,Read,BufReader};
use std::fs::File;

use awk::evaluator::*;
use awk::parser::*;

fn main() {
    let mut script = String::new();
    match File::open("test.rawk") {
        Ok(mut f) =>{
            f.read_to_string(&mut script);
            format!("{:?}", f);
        },
        Err(e) => {
            format!("{}", e);
            panic!("hello")
        }
    }

    let actions = actions_from_script(script);
    let mut ev = Evaluator {
        actions: actions,
        state: EvaluatorState {
            fields: Vec::new(),
            variables: std::collections::HashMap::new()
        }
    };
    println!("{:?}", ev);

    let stdin = io::stdin();
    let mut linebuf = BufReader::new(stdin.lock());
    process_lines(&mut ev, &mut linebuf);
}
