use ast::Condition;

use nom::{alphanumeric,space,line_ending};
use nom::types::CompleteStr;

use regex::Regex;

named!(regex<CompleteStr, Regex>,
       map_res!(
           delimited!(
               tag!("/"),
               alphanumeric,
               tag!("/")),
               |s: CompleteStr| Regex::new(&s.to_string())));

named!(rmatch<CompleteStr, Condition>,
       map!(
           regex,
           |s| Condition::Match(s)));

named!(spacing<CompleteStr,()>, do_parse!(char!(',') >> opt!(alt!(space | line_ending)) >> ()));

named!(range<CompleteStr, Condition>,
       map!(
           separated_pair!(regex, spacing, regex),
           |(a,b)| Condition::Range(a,b)));

named!(pub parse_condition<CompleteStr, Condition>,
       preceded!(opt!(space),
       alt!(
           value!(Condition::Begin, tag!("BEGIN")) 
           | value!(Condition::End, tag!("END")) 
           | range
           | rmatch
           )));
