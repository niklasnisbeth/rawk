use ast::Statement;

use parser::{parse_expression,varid};

use nom::types::CompleteStr;
use nom::{space,line_ending};

named!(statement_print<CompleteStr, Statement>,
       map!(ws!(preceded!(tag!("print "),
                      parse_expression)),
            |e| Statement::Print(Box::new(e))));

named!(statement_assignment<CompleteStr, Statement>,
       map!(ws!(separated_pair!(varid, char!('='), 
                                parse_expression)),
            |(id, expr)| Statement::Assignment(id, Box::new(expr))));

named!(pub parse_statement<CompleteStr, Statement>,
       terminated!(alt!(statement_assignment
                        | statement_print),
                   char!(';')));

named!(pub parse_statement_block<CompleteStr, Vec<Statement> >,
       do_parse!(
               char!('{') >> 
               opt!(space) >>
               statements: many1!(parse_statement) >>
               opt!(space) >>
               char!('}') >>
               alt!(line_ending | eof!()) >>
               (statements)));
