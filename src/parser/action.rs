use ast::{Condition, Action};

use parser::{parse_statement_block,parse_condition};

use nom::types::CompleteStr;
use nom::{space};

named!(pub parse_action<CompleteStr, Action>,
     map!(pair!(opt!(terminated!(parse_condition, space)), parse_statement_block),
     |(mcond, statements)| match mcond {
          Some(cond) => Action { condition: cond, block: statements },
          None => Action { condition: Condition::All, block: statements }
     }));

named!(pub parse_actions<CompleteStr, Vec<Action> >,
       many1!(parse_action));

pub fn actions_from_script(script: String) -> Vec<Action> {
    match parse_actions(CompleteStr(&script)).unwrap() { 
        (CompleteStr(_), actions) => actions,
    }
}
