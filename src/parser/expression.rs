use ast::Expression;

use values::Value::*;

use nom::types::CompleteStr;

use nom::{digit,alphanumeric, is_alphabetic};

use std::str::FromStr;

enum InfixOp {
    Plus,
    Minus,
    LessThan,
    GreaterThan
}

named!(constant_int<CompleteStr, Expression>,
       map!(
           map_res!(digit, 
                    |s: CompleteStr| i64::from_str(s.as_ref())), 
           |i| Expression::Constant(Numeric(i))));

named!(constant_fieldref<CompleteStr, Expression>,
       map!(
           map_res!(preceded!(char!('$'), digit), 
                    |s: CompleteStr| i64::from_str(s.as_ref())), 
           |i| Expression::FieldRef(i)));

named!(one_char<CompleteStr, char>,
       map_opt!(take!(1),
                |c: CompleteStr| {
                    let cp = c.as_bytes()[0];
                    if is_alphabetic(cp) { 
                        Some(cp as char) 
                    } else { 
                        None 
                    }
                }));
                 

named!(pub varid<CompleteStr, String>,
       do_parse!(fst: one_char >>
                 rst: alphanumeric >>
                 (format!("{}{}", fst, rst.as_ref()))));

named!(constant_varref<CompleteStr, Expression>,
       map!(varid,
            |x| Expression::VarRef(x)));

named!(simplex<CompleteStr, Expression>, 
       alt!(
           constant_int
           | constant_fieldref
           | constant_varref));

named!(op<CompleteStr, InfixOp>,
       alt!(
           value!(InfixOp::Plus, char!('+'))
           | value!(InfixOp::Minus, char!('-'))
           | value!(InfixOp::LessThan, char!('>'))
           | value!(InfixOp::GreaterThan, char!('<'))));

fn expr1(l: Expression, x: Option<(InfixOp, Expression)>) -> Expression {
    match x {
        None => l,
        Some((InfixOp::Plus, r)) => Expression::Plus(Box::new(l), Box::new(r)),
        Some((InfixOp::Minus, r)) => Expression::Minus(Box::new(l), Box::new(r)),
        Some((InfixOp::LessThan, r)) => Expression::LessThan(Box::new(l), Box::new(r)),
        Some((InfixOp::GreaterThan, r)) => Expression::GreaterThan(Box::new(l), Box::new(r))
    }
}

named!(pub parse_expression<CompleteStr, Expression>,
       do_parse!(l: simplex >> 
                 x: opt!(pair!(op, parse_expression)) >>
                 (expr1(l, x))));
